#include <msp430.h> 
#include <stdint.h>
#include <string.h>
#include "lib/gpio.h"
#include "lib/i2c.h"
#include "lib/oled.h"
#include "lib/fonts.h"
#include "lib/temperatuur.h"
#include <stdio.h>
//#pragma vector = TIMER0_A1_VECTOR


// Tabel met sinuswaarden. Sneller dan berekenen!
const uint8_t sine[] =
{
    0x7,0x8,0xa,0xb,0xc,0xd,0xd,0xe,
    0xe,0xe,0xd,0xd,0xc,0xb,0xa,0x8,
    0x7,0x6,0x4,0x3,0x2,0x1,0x1,0x0,
    0x0,0x0,0x1,0x1,0x2,0x3,0x4,0x6,
};

/*__interrupt void optellen()
{

}*/

int waarde1 = 48; // Omdat er gebruikt gemaakt wordt van draw character functie, zijn de waarde in ASCII formaat
int waarde2 = 48;
int waarde3 = 48;
int waarde4 = 48;
int waarde5 = 48;
int vertraging = 0;

int optellen()
{
    oledDrawFontChar(3, 121, waarde1, small);
    oledDrawFontChar(3, 115, waarde2, small);
    oledDrawFontChar(3, 109, waarde3, small);
    oledDrawFontChar(3, 103, waarde4, small);
    oledDrawFontChar(3, 97, waarde5, small);
    while (1)
    {
        while (TA0R < 62500)
        {
            /*wacht*/
        }

        if (vertraging > 32766)
        {
         vertraging = 0;

        waarde1++;

        if (waarde1 == 58)
        {
            waarde1 = 48;
            waarde2++;
        }

        if (waarde2 == 58)
        {
            waarde2 = 48;
            waarde3++;
        }

        if (waarde3 == 58)
        {
            waarde3 = 48;
            waarde4++;
        }

        if (waarde4 == 58)
        {
            waarde4 = 48;
            waarde5++;
        }

        if (waarde5 == 58)
        {
            waarde1 = 48;
            waarde2 = 48;
            waarde3 = 48;
            waarde4 = 48;
            waarde5 = 48;
        }
        oledDrawFontChar(3, 121, waarde1, small);
        oledDrawFontChar(3, 115, waarde2, small);
        oledDrawFontChar(3, 109, waarde3, small);
        oledDrawFontChar(3, 103, waarde4, small);
        oledDrawFontChar(3, 97, waarde5, small);

        }
        else {
            vertraging++;
        }
}
}
void main(void) {
    WDTCTL = WDTPW | WDTHOLD; // Stop watchdog timer

    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ; // Set range
    DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation

    /* Het display heeft na het aansluiten van de voedingsspanning even
     * de tijd nodig om op te starten voordat hij aangestuurd kan worden.
     * We wachten hier 20ms.
     */
    __delay_cycles(320000);

    // Zet display aan
    oledInitialize();
    // Eventueel flippen
    oledSetOrientation(FLIPPED);
    // Begin met een leeg scherm
    oledClearScreen();


    oledPrint(15, 1, "Afstand", small);
    oledPrint(100, 1, "Tijd", small);
//    oledPrint(105, 3, "sec.", small);
    oledPrint(15, 3, "R", small);
    oledPrint(15, 5, "G", small);
    oledPrint(15, 7, "B", small);



    TA0CTL = TASSEL_2 | MC_2 | ID_3;

    TA0R = 0;

    while(1)
    {

    optellen();
    }

//    uint8_t kolom, n = 0;

//    while (1){
//        n++;
//        n = n % 32;
//
//        /* Update de buffer bij elke kolom.
//         * Schuif hem op met het aantal iteraties van de
//         * while loop modulus 32 zodat we binnen het
//         * bereik van de sine-array blijven.
//         *
//         */
//        for (kolom = 0; kolom < 128; kolom++)
//        {
//            oledSetBufferPixel(kolom, sine[(kolom + n + 1) % 32]); // Plaats nieuwe sinus.
//            oledClearBufferPixel(kolom, sine[(kolom + n) % 32]); // Verwijder eerdere sinus.
//        }
//
//        // Schrijf framebuffer naar het oleddisplay.
//        oledWriteBuffer(3,2);
//
//        __delay_cycles(160000);
//    }
}
